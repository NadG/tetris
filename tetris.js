console.log('tetris.js loaded');

// scriviamo nel canvas
const screen = document.getElementById('screen')
// definizione constest che mi  permetterà di disegnare sul canvas.
const ctx = screen.getContext('2d');
// Webgl ha 2 tipi di context: 2d e 3d

// scale ti permette di scalare i px. Qui 1 px diventa 20px
ctx.scale(10, 10);
// dire al context come deve
ctx.fillStyle="#455a64";
// definizione di un rettangolo che va da 0,0 da in alto a sx, e poi definisci quanto
// è largo e quanto è alto;. Ecco che hai definito il canvas
ctx.fillRect(0,0, screen.width, screen.height);

// definizione dei blocchettini. w e h hanno come unità di misura il px
ctx.fillStyle = 'gold';
// grazie allo scale definito in precedenza, w1 e h1 corrrispondono a 20px invece che 1px
ctx.fillRect(5, 5, 1, 1)